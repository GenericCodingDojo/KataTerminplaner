﻿using Terminplaner.Features;
using Terminplaner.Model;

namespace Terminplaner
{
    public static class Terminplaner
    {
        public static void Einplanen(this Kalender kalender, Termin termin)
        {
            kalender.Einfuegen(termin);
            kalender.UeberschneidungenKorrigieren();
        }
    }
}
