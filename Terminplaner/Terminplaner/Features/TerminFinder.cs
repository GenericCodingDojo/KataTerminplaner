﻿using System;
using System.Collections.Generic;
using System.Linq;
using Terminplaner.Model;

namespace Terminplaner.Features
{
    public class TerminFinder
    {
        private Func<DateTime> now;

        public TerminFinder() : this(() => DateTime.Now) {}

        internal TerminFinder(Func<DateTime> now)
        {
            this.now = now;
        }
        

        public Termin FindeFreienTermin(TimeSpan dauer, IEnumerable<Kalender> kalender)
        {
            var zusammengefassterKalender = new ZusammengefassterKalender(kalender);
            var termin = FindeFreienTermin(dauer, zusammengefassterKalender);
            return termin;
        }

        public Termin FindeFreienTermin(TimeSpan dauer, Kalender kalender)
        {
            var termine = ErmittleTermineNachJetzt(kalender);

            if (!termine.Any() || ZeitBisErsterTermin(now(), termine) >= dauer)
            {
                return new Termin() {Start = now(), Ende = now() + dauer};
            }

            return FindeFreienTermin(dauer, termine);
        }

        internal Termin FindeFreienTermin(TimeSpan dauer, IEnumerable<Termin> termine)
        {
            Termin vorherigerTermin = null;

            foreach (var termin in termine)
            {
                if (vorherigerTermin == null)
                {
                    vorherigerTermin = termin;
                    continue;
                }

                if (vorherigerTermin.Ende >= termin.Ende)
                {
                    continue;
                }

                if (vorherigerTermin.DauerBis(termin) >= dauer)
                {
                    return ErstelleNeuenTerminNach(vorherigerTermin, dauer);
                }
                vorherigerTermin = termin;
            }
            return ErstelleNeuenTerminNach(vorherigerTermin, dauer);
        }

        private TimeSpan ZeitBisErsterTermin(DateTime ab, IEnumerable<Termin> termine)
        {
            return termine.First().Start - ab;
        }

        internal IEnumerable<Termin> ErmittleTermineNachJetzt(IEnumerable<Termin> termine)
        {
            return termine.Where(x => x.Ende > now());
        }

        private Termin ErstelleNeuenTerminNach(Termin t, TimeSpan dauer)
        {
            return new Termin() { Start = t.Ende, Ende = t.Ende + dauer };
        }
    }
}
