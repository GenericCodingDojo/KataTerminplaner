﻿using System.Linq;
using Terminplaner.Model;

namespace Terminplaner.Features
{
    public static class TerminEinfuegen
    {
        public static void Einfuegen(this Kalender kalender, Termin neuerTermin)
        {
            var vorherigerTermin = kalender.GetErsterTerminVor(neuerTermin);

            if (neuerTermin.MussAngehaengtWerdenAn(vorherigerTermin))
            {
                neuerTermin.AnhaengenAn(vorherigerTermin);
            }

            kalender.Add(neuerTermin);
        }

        internal static bool MussAngehaengtWerdenAn(this Termin termin, Termin vorherigerTermin)
        {
            return vorherigerTermin != null &&
                   (
                       vorherigerTermin.HatUeberschneidungMit(termin) ||
                       vorherigerTermin.BeginntZurGleichenZeitWie(termin)
                   );
        }
        
        internal static Termin GetErsterTerminVor(this Kalender kalender, Termin termin)
        {
            return kalender
                .OrderBy(x => x.Start)
                .LastOrDefault(x => x.Start <= termin.Start && x != termin);
        }

        internal static void UeberschneidungenKorrigieren(this Kalender kalender)
        {
            Termin vorgaenger = null;

            foreach (var termin in kalender)
            {
                if (vorgaenger != null && vorgaenger.HatUeberschneidungMit(termin))
                {
                    termin.AnhaengenAn(vorgaenger);
                }

                vorgaenger = termin;
            }
        }
    }
}
