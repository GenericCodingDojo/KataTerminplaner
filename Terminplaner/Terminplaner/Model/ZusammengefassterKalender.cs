﻿using System.Collections.Generic;

namespace Terminplaner.Model
{
    public class ZusammengefassterKalender : Kalender
    {
        public ZusammengefassterKalender(IEnumerable<Kalender> kalender)
        {
            this.termine = new List<Termin>();

            foreach (var k in kalender)
            {
                this.termine.AddRange(k);
            }

            this.termine.Sort();
        }
    }
}
