﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Terminplaner.Model
{
    public class Kalender : IEnumerable<Termin>
    {
        protected List<Termin> termine;

        public Kalender()
        {
            termine = new List<Termin>();
        }


        public IEnumerator<Termin> GetEnumerator()
        {
            return termine.GetEnumerator();
        }

        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        

        internal void Add(Termin neuerTermin)
        {
            this.termine.Add(neuerTermin);
            this.termine.Sort();
        }
    }
}
