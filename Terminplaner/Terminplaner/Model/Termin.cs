﻿using System;

namespace Terminplaner.Model
{
    public class Termin : IComparable<Termin>
    {
        public DateTime Start { get; set; }

        public DateTime Ende { get; set; }

        public int CompareTo(Termin other)
        {
            return this.Start.CompareTo(other.Start);
        }

        public void AnhaengenAn(Termin vorgaenger)
        {
            var start = vorgaenger.Ende;
            var dauer = Ende - Start;

            this.Start = start;
            this.Ende = start + dauer;
        }

        public bool HatUeberschneidungMit(Termin t)
        {
            return this.Start < t.Ende && t.Start < this.Ende;
        }

        public bool BeginntZurGleichenZeitWie(Termin t)
        {
            return this.Start == t.Start;
        }

        public TimeSpan DauerBis(Termin t)
        {
            var dauer = t.Start - this.Ende;

            if (dauer.Hours < 0)
            {
                return new TimeSpan(0);
            }
            return dauer;
        }

        public override string ToString()
        {
            return $"{Start} - {Ende}";
        }
    }
}
