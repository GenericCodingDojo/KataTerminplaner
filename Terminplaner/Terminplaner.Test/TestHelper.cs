﻿using FluentAssertions;
using Terminplaner.Model;

namespace Terminplaner.Test
{
    public static class TestHelper
    {

        public static void ShouldBeTheSameAs(this Termin t1, Termin t2)
        {
            t1.Start.Should().Be(t2.Start);
            t1.Ende.Should().Be(t2.Ende);
        }

        public static void ShouldNotBeTheSameAs(this Termin t1, Termin t2)
        {
            t1.Start.Should().NotBe(t2.Start);
            t1.Ende.Should().NotBe(t2.Ende);
        }
    }
}
