﻿using System;
using FluentAssertions;
using NUnit.Framework;
using Terminplaner.Model;

namespace Terminplaner.Test.Model
{
    [TestFixture]
    public class TerminTests
    {

        [Test]
        public void SollTerminAnhaengen()
        {
            var sampleDate = SampleData.GetSampleDate();

            var terminZuerst = new Termin() {Start = sampleDate, Ende = sampleDate.AddHours(8) };
            var terminZuletzt = new Termin() {Start = sampleDate.AddHours(1), Ende = sampleDate.AddHours(9)};

            terminZuletzt.AnhaengenAn(terminZuerst);

            terminZuletzt.Start.Should().Be(terminZuerst.Ende);
            terminZuletzt.Ende.Should().Be(terminZuletzt.Start.AddHours(8));
        }

        [TestCase(8, 9, 10, 11, false)]
        [TestCase(8, 9, 9, 10, false)]
        [TestCase(8, 10, 9, 11, true)]
        [TestCase(8, 10, 8, 10, true)]
        [TestCase(8, 12, 9, 10, true)]
        public void SollUeberschneidungErkennen(int t1Start, int t1Ende, int t2Start, int t2Ende, bool ueberschneidung)
        {
            var t1 = SampleData.CreateTermin(t1Start, t1Ende);
            var t2 = SampleData.CreateTermin(t2Start, t2Ende);

            t1.HatUeberschneidungMit(t2).Should().Be(ueberschneidung);
            t2.HatUeberschneidungMit(t1).Should().Be(ueberschneidung);
        }

        [TestCase(8, 10, 12, 14, 2)]
        [TestCase(11, 12, 8, 9, 0)]
        [TestCase(20, 21, 21, 22, 0)]
        public void SollDauerBisBerechnen(int t1Start, int t1Ende, int t2Start, int t2Ende, int dauer)
        {
            var t1 = SampleData.CreateTermin(t1Start, t1Ende);
            var t2 = SampleData.CreateTermin(t2Start, t2Ende);

            t1.DauerBis(t2).Should().Be(TimeSpan.FromHours(dauer));
        }
    }
}
