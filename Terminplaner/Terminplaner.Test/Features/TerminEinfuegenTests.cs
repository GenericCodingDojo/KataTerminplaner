﻿using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using Terminplaner.Features;
using Terminplaner.Model;

namespace Terminplaner.Test.Features
{
    [TestFixture]
    public class TerminEinfuegenTests
    {
        [Test]
        public void SollErkennenDassAngehaengtWerdenMuss()
        {
            var t1 = SampleData.CreateT1MitUeberschneidungZuT2();
            var t2 = SampleData.CreateT2MitUeberschneidungZuT1();

            t2.MussAngehaengtWerdenAn(t1).Should().BeTrue();
            t1.MussAngehaengtWerdenAn(t2).Should().BeTrue();
        }

        [Test]
        public void SollErkennenDassNichtAngehaengtWerdenMuss()
        {
            var t1 = SampleData.CreateT1MitUeberschneidungZuT2();
            var t2 = SampleData.CreateT2MitUeberschneidungZuT1();
            var t3 = SampleData.CreateT3OhneUberschneidung();

            t3.MussAngehaengtWerdenAn(t1).Should().BeFalse();
            t3.MussAngehaengtWerdenAn(t2).Should().BeFalse();
        }

        [Test]
        public void SollErsterTerminErmitteln()
        {
            var kalender = SampleData.CreateKalender();

            kalender.GetErsterTerminVor(kalender.Last()).Should().Be(kalender.ElementAt(1));
            kalender.GetErsterTerminVor(kalender.First()).Should().Be(null);
        }

        [Test]
        public void SollTerminEinfuegenMitUeberschneidung()
        {
            var kalender = SampleData.CreateKalender();

            var neuerTermin = SampleData.CreateNeuerTerminFuerKalenderMitUeberschneidung();

            var anhaengeTermin = kalender.Last();

            kalender.Einfuegen(neuerTermin);

            kalender.Count().Should().Be(4);

            kalender.Last().Should().Be(neuerTermin);

            neuerTermin.Start.Should().Be(anhaengeTermin.Ende);
            neuerTermin.Ende.Should().Be(anhaengeTermin.Ende.AddHours(2));
        }

        [Test]
        public void SollTerminEinfuegenOhneUeberschneidung()
        {
            var kalender = SampleData.CreateKalender();

            var neuerTermin = SampleData.CreateNeuerTerminFuerKalenderOhneUeberschneidung();

            var anhaengeTermin = kalender.Last();

            kalender.Einfuegen(neuerTermin);

            kalender.Count().Should().Be(4);

            kalender.Last().Should().Be(neuerTermin);

            neuerTermin.Start.Should().Be(SampleData.CreateNeuerTerminFuerKalenderOhneUeberschneidung().Start);
            neuerTermin.Ende.Should().Be(SampleData.CreateNeuerTerminFuerKalenderOhneUeberschneidung().Ende);
        }

        [Test]
        public void SollUeberschneidungKorrigierenVonZweiTerminen()
        {
            var kalender = new Kalender();
            kalender.Add(SampleData.CreateT1MitUeberschneidungZuT2());
            kalender.Add(SampleData.CreateT2MitUeberschneidungZuT1());

            kalender.UeberschneidungenKorrigieren();

            var t1 = kalender.First();
            var t2 = kalender.Last();

            t1.ShouldBeTheSameAs(SampleData.CreateT1MitUeberschneidungZuT2());
            t2.ShouldNotBeTheSameAs(SampleData.CreateT2MitUeberschneidungZuT1());

            t2.Start.Should().Be(t1.Ende);
        }

        [Test]
        public void SollUeberschneidungKorrigierenVonZweiGleichenTerminen()
        {
            var kalender = new Kalender();
            kalender.Add(SampleData.CreateSimpleTermin());
            kalender.Add(SampleData.CreateSimpleTermin());

            kalender.UeberschneidungenKorrigieren();

            var t1 = kalender.First();
            var t2 = kalender.Last();

            t1.ShouldBeTheSameAs(SampleData.CreateSimpleTermin());
            t2.Start.Should().Be(t1.Ende);
        }

        [Test]
        public void SollUeberschneidungKorrigierenVonZweiTerminenUndFolgeueberschneidungDurchVerschiebung()
        {
            var kalender = new Kalender();
            kalender.Add(SampleData.CreateTermin(8,10));
            kalender.Add(SampleData.CreateTermin(9,11));
            kalender.Add(SampleData.CreateTermin(12,14));

            kalender.UeberschneidungenKorrigieren();

            var t1 = kalender.ElementAt(0);
            var t2 = kalender.ElementAt(1);
            var t3 = kalender.ElementAt(2);

            t1.ShouldBeTheSameAs(SampleData.CreateTermin(8, 10));
            t2.Start.Should().Be(t1.Ende);
            t3.Start.Should().Be(t2.Ende);
        }

        [Test]
        public void SollTerminEinfuegen()
        {
            var kalender = new Kalender();
            kalender.Add(SampleData.CreateTermin(8, 10));
            kalender.Add(SampleData.CreateTermin(12, 14));

            kalender.Einplanen(SampleData.CreateTermin(9, 11));

            var t1 = kalender.ElementAt(0);
            var t2 = kalender.ElementAt(1);
            var t3 = kalender.ElementAt(2);

            t1.ShouldBeTheSameAs(SampleData.CreateTermin(8, 10));
            t2.Start.Should().Be(t1.Ende);
            t3.Start.Should().Be(t2.Ende);
        }
    }
}
