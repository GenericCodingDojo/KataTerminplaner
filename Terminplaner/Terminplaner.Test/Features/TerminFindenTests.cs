﻿using System;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using Terminplaner.Features;
using Terminplaner.Model;
using System.Collections.Generic;

namespace Terminplaner.Test.Features
{
    [TestFixture]
    public class TerminFindenTests
    {
        private TerminFinder terminFinder;

        [OneTimeSetUp]
        public void Init()
        {
            this.terminFinder = new TerminFinder(() => Now);
        }

        [Test]
        public void SollFreienTerminAmEndeFinden()
        {
            var termine = new List<Termin>();
            termine.Add(ErstelleTerminHeute(8,10));
            termine.Add(ErstelleTerminHeute(10,11));
            termine.Add(ErstelleTerminHeute(11,12));

            var dauer = new TimeSpan(1, 0, 0);

            var termin = terminFinder.FindeFreienTermin(dauer, termine);

            termin.ShouldBeTheSameAs(ErstelleTerminHeute(12, 13));
        }

        [Test]
        public void SollFreienTerminMitLueckenAmEndeFinden()
        {
            var termine = new List<Termin>();
            termine.Add(ErstelleTerminHeute(8, 10));
            termine.Add(ErstelleTerminHeute(11, 12));
            termine.Add(ErstelleTerminHeute(13, 14));

            var dauer = new TimeSpan(2, 0, 0);

            var termin = terminFinder.FindeFreienTermin(dauer, termine);

            termin.ShouldBeTheSameAs(ErstelleTerminHeute(14, 16));
        }

        [Test]
        public void SollFreienTerminInLueckeFinden()
        {
            var termine = new List<Termin>();
            termine.Add(ErstelleTerminHeute(8, 10));
            termine.Add(ErstelleTerminHeute(11, 12));
            termine.Add(ErstelleTerminHeute(13, 14));

            var dauer = new TimeSpan(1, 0, 0);

            var termin = terminFinder.FindeFreienTermin(dauer, termine);

            termin.ShouldBeTheSameAs(ErstelleTerminHeute(10, 11));
        }

        [Test]
        public void SollFreienTerminAmEndeFindenMitTerminueberschneidungen()
        {
            var termine = new List<Termin>();
            termine.Add(ErstelleTerminHeute(8, 10));
            termine.Add(ErstelleTerminHeute(9, 11));
            termine.Add(ErstelleTerminHeute(11, 12));
            termine.Add(ErstelleTerminHeute(11, 13));
            termine.Add(ErstelleTerminHeute(13, 15));
            termine.Add(ErstelleTerminHeute(14, 15));

            var dauer = new TimeSpan(1, 0, 0);

            var termin = terminFinder.FindeFreienTermin(dauer, termine);

            termin.ShouldBeTheSameAs(ErstelleTerminHeute(15, 16));
        }

        [Test]
        public void SollFreienTerminMitLueckenAmEndeFindenMitTerminueberschneidungen()
        {
            var termine = new List<Termin>();
            termine.Add(ErstelleTerminHeute(6, 11));
            termine.Add(ErstelleTerminHeute(8, 9));
            termine.Add(ErstelleTerminHeute(11, 12));
            termine.Add(ErstelleTerminHeute(12, 13));
            termine.Add(ErstelleTerminHeute(13, 16));
            termine.Add(ErstelleTerminHeute(15, 16));

            var dauer = new TimeSpan(2, 0, 0);

            var termin = terminFinder.FindeFreienTermin(dauer, termine);

            termin.ShouldBeTheSameAs(ErstelleTerminHeute(16, 18));
        }

        [Test]
        public void SollFreienTerminInLueckeFindenMitTerminueberschneidungen()
        {
            var termine = new List<Termin>();

            termine.Add(ErstelleTerminHeute(6, 10));
            termine.Add(ErstelleTerminHeute(8, 9));
            termine.Add(ErstelleTerminHeute(8, 9));
            termine.Add(ErstelleTerminHeute(11, 12));
            termine.Add(ErstelleTerminHeute(11, 14));
            termine.Add(ErstelleTerminHeute(13, 15));
            termine.Add(ErstelleTerminHeute(17, 18));
            termine.Add(ErstelleTerminHeute(17, 20));

            var dauer = new TimeSpan(2, 0, 0);

            var termin = terminFinder.FindeFreienTermin(dauer, termine);

            termin.ShouldBeTheSameAs(ErstelleTerminHeute(15, 17));
        }

        [Test]
        public void SollFreienTerminInZusammengefasstemKalenderVorAllenTerminenFinden()
        {

            var kalender1 = new Kalender();
            kalender1.Add(ErstelleTerminHeute(8, 9));
            kalender1.Add(ErstelleTerminHeute(11, 14));
            kalender1.Add(ErstelleTerminHeute(17, 20));

            var kalender2 = new Kalender();
            kalender2.Add(ErstelleTerminHeute(8, 9));
            kalender2.Add(ErstelleTerminHeute(11, 12));

            kalender2.Add(ErstelleTerminHeute(17, 18));

            var dauer = new TimeSpan(2, 0, 0);

            this.Now = DateTime.Today.AddHours(4);

            var termin = terminFinder.FindeFreienTermin(dauer, new[] { kalender1, kalender2 });

            termin.ShouldBeTheSameAs(ErstelleTerminHeute(4, 6));
        }

        [Test]
        public void SollTermineAbHeuteErmitteln()
        {
            var kalender1 = new Kalender();
            kalender1.Add(ErstelleTerminHeute(8, 18));

            var kalender2 = new Kalender();
            kalender2.Add(ErstelleTerminHeute(6, 9));
            kalender2.Add(ErstelleTerminHeute(11, 12));

            kalender2.Add(ErstelleTerminHeute(17, 18));

            var kalender3 = new Kalender();
            kalender3.Add(ErstelleTerminHeute(6, 10));
            kalender3.Add(ErstelleTerminHeute(13, 15));

            var zusammengefassterKalender = new ZusammengefassterKalender(new [] {kalender1, kalender2, kalender3});

            this.Now = DateTime.Today.AddHours(10);

            terminFinder.ErmittleTermineNachJetzt(zusammengefassterKalender).Count().Should().Be(4);
        }

        [Test]
        public void SollTermineAbErmitteln()
        {
            var t1 = SampleData.CreateTermin(8, 10);
            var t2 = SampleData.CreateTermin(10, 12);
            var t3 = SampleData.CreateTermin(13, 15);

            var kalender = new Kalender();
            kalender.Add(t1);
            kalender.Add(t2);
            kalender.Add(t3);

            this.Now = t2.Ende - TimeSpan.FromHours(1);

            terminFinder.ErmittleTermineNachJetzt(kalender).ShouldBeEquivalentTo(new []{t2,t3});
        }


        [Test]
        public void SollTerminFindenMitLeeremKalender()
        {

            var kalender = new Kalender();

            this.Now = new DateTime(2017,1,1,8,0,0);

            var dauer = new TimeSpan(2, 0, 0);

            var termin = terminFinder.FindeFreienTermin(dauer, kalender);
            termin.Start.Should().Be(this.Now);
            termin.Ende.Should().Be(this.Now + dauer);
        }

        private static Termin ErstelleTerminHeute(int from, int to)
        {
            var today = DateTime.Today;
            return new Termin()
            {
                Start = new DateTime(today.Year, today.Month, today.Day, from, 0, 0),
                Ende = new DateTime(today.Year, today.Month, today.Day, to, 0, 0)
            };
        }

        private DateTime Now { get; set; }
    }

}
