﻿using System;
using Terminplaner.Model;

namespace Terminplaner.Test
{
    public static class SampleData
    {
        public static Termin CreateT1MitUeberschneidungZuT2()
        {
            return new Termin()
            {
                Start = new DateTime(2017, 1, 1, 10, 0, 0),
                Ende = new DateTime(2017, 1, 1, 16, 0, 0)
            };
        }

        public static Termin CreateT2MitUeberschneidungZuT1()
        {
            return new Termin()
            {
                Start = new DateTime(2017, 1, 1, 14, 0, 0),
                Ende = new DateTime(2017, 1, 1, 18, 0, 0)
            };
        }

        public static Termin CreateT3OhneUberschneidung()
        {
            return new Termin()
            {
                Start = new DateTime(2017, 1, 1, 20, 0, 0),
                Ende = new DateTime(2017, 1, 1, 22, 0, 0)
            };
        }

        public static Kalender CreateKalender()
        {
            return CreateSampleKalender();
        }

        private static Kalender CreateSampleKalender()
        {
            var kalender = new Kalender();

            kalender.Add(new Termin()
            {
                Start = new DateTime(2017, 1, 1, 8, 0, 0),
                Ende = new DateTime(2017, 1, 1, 9, 0, 0)
            } );

            kalender.Add(new Termin()
            {
                Start = new DateTime(2017, 1, 1, 10, 0, 0),
                Ende = new DateTime(2017, 1, 1, 12, 0, 0)
            });

            kalender.Add(new Termin()
            {
                Start = new DateTime(2017, 1, 1, 13, 0, 0),
                Ende = new DateTime(2017, 1, 1, 15, 0, 0)
            });

            return kalender;
        }

        public static Termin CreateNeuerTerminFuerKalenderMitUeberschneidung()
        {
            return new Termin()
            {
                Start = new DateTime(2017, 1, 1, 14, 0, 0),
                Ende = new DateTime(2017, 1, 1, 16, 0, 0)
            };
        }

        public static Termin CreateNeuerTerminFuerKalenderOhneUeberschneidung()
        {
            return new Termin()
            {
                Start = new DateTime(2017, 1, 1, 17, 0, 0),
                Ende = new DateTime(2017, 1, 1, 18, 0, 0)
            };
        }


        public static Termin CreateSimpleTermin()
        {
            return new Termin()
            {
                Start = new DateTime(2017, 1, 1, 8, 0, 0),
                Ende = new DateTime(2017, 1, 1, 10, 0, 0)
            };
        }

        public static Termin CreateTermin(int from, int to)
        {
            return new Termin()
            {
                Start = new DateTime(2017, 1, 1, from, 0, 0),
                Ende = new DateTime(2017, 1, 1, to, 0, 0)
            };

        }

        public static DateTime GetSampleDate()
        {
            return new DateTime(2017, 1, 1, 8, 0, 0);
        }
    }
}
